<!-- TOC -->

- [1. README](#1-readme)
    - [1.1. [Onion Architecture for Simple CRUD based application](./Src/SimpleOnionArchitecture/README.md)](#11-onion-architecture-for-simple-crud-based-applicationsrcsimpleonionarchitecturereadmemd)
    - [1.2. Domain Driven Design for task based application](#12-domain-driven-design-for-task-based-application)

<!-- /TOC -->
# 1. README

## 1.1. [Onion Architecture for Simple CRUD based application](./Src/SimpleOnionArchitecture/README.md)

## 1.2. Domain Driven Design for task based application