﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Core
{
    public abstract class ReadModelBase<TKey>
    {
        public TKey Id { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public long CreatedByUserId { get; set; }     // use to query/join
        public string CreatedByUserName { get; set; } // Use to display

        public DateTimeOffset UpdatedDate { get; set; }
        public long UpdatedByUserId { get; set; } // use to query/join
        public string UpdatedByUserName { get; set; } // Use to display
    }


}
