﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domains
{
    /// <summary>
    /// This is assemply marker
    /// It'll help you in IoC registeration like: services.AddService(typeof(AssemplyMarker))
    /// </summary>
    public sealed class AssemplyMarker
    {
    }
}
