﻿using ApplicationCore.Identity.Entities;
using ApplicationCore.Identity.Models;
using ApplicationCore.Identity.Repositories;
using Groove.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace ApplicationCore.Identity.Services
{
    public class UserAuthenticationService : IUserAuthenticationService
    {
        private readonly SignInManager<User>  _signInManager;
        private readonly UserManager<User>  _userManager;
        private readonly IUserRepository  _userRepository;

        public UserAuthenticationService(IUserRepository userRepository, SignInManager<User> signInManager, UserManager<User> userManager)
        {
            _userRepository = userRepository;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        public async Task<SignInResultModel> PasswordSignInAsync(string userName, string password, bool lockoutOnFailure)
        {
            var user = await _userRepository.GetByUserNameAsync(userName);

            if (user == null)
            {
                return new SignInResultModel
                {
                    Succeeded = false
                };
            }

            var loginResult = new SignInResultModel(await _signInManager.CheckPasswordSignInAsync(user, password, lockoutOnFailure));
            if (!loginResult.Succeeded)
            {
                return loginResult;
            }

            loginResult.Roles = await _userManager.GetRolesAsync(user);
            loginResult.UserIdentity = new UserIdentity<long>
            {
                Id = user.Id,
                UserName = user.UserName
            };

            return loginResult;
        }
    }
}