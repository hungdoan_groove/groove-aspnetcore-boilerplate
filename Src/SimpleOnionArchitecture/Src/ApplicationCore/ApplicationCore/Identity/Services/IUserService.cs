﻿using ApplicationCore.Identity.Models;
using Groove.AspNetCore.Identity;
using System.Threading.Tasks;

namespace ApplicationCore.Identity.Services
{
    public interface IUserService
    {
        Task<long> CreateUserAsync(UserCreateModel model, UserIdentity<long> issuer);

        Task UpdateUserAsync(long id, UserUpdateModel model, UserIdentity<long> issuer);
    }
}