﻿using ApplicationCore.Identity.Models;
using System.Threading.Tasks;

namespace ApplicationCore.Identity.Services
{
    public interface IUserAuthenticationService
    {
        Task<SignInResultModel> PasswordSignInAsync(string userName, string password, bool lockoutOnFailure);
    }
}