﻿using AutoMapper;
using ApplicationCore.Identity.Entities;
using ApplicationCore.Identity.Models;
using Groove.AspNetCore.Common.Exceptions;
using Groove.AspNetCore.Identity;
using Groove.AspNetCore.Common.Messages;
using Groove.AspNetCore.UnitOfWork;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationCore.Identity.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _uow;
        private readonly IGenericRepository<User,long> _userGenericRepository;
        private readonly UserManager<User> _userManagement;

        public UserService(
            IMapper mapper,
            IGenericRepository<User, long> userGenericRepository,
            UserManager<User> userManagement,
            IUnitOfWork uow)
        {
            _userGenericRepository = userGenericRepository;
            _userManagement = userManagement;
            _mapper = mapper;
            _uow = uow;
        }

        public async Task<long> CreateUserAsync(UserCreateModel model, UserIdentity<long> issuer)
        {
            var user = _mapper.Map<User>(model);
            user.CreateBy(issuer).UpdateBy(issuer);

            var identityResult = await _userManagement.CreateAsync(user, model.Password);

            if (!identityResult.Succeeded)
            {
                throw CreateException(identityResult.Errors);
            }

            return user.Id;
        }

        public async Task UpdateUserAsync(long id, UserUpdateModel model, UserIdentity<long> issuer)
        {
            var user = await _userGenericRepository.GetEntityByIdAsync(id);

            _mapper.Map(model, user);
            user.UpdateBy(issuer);

            _userGenericRepository.Update(user);
            await _uow.SaveChangesAsync();
        }

        private UserDefinedException CreateException(IEnumerable<IdentityError> errors)
        {
            var exception = new UserDefinedException();
            exception.UserDefinedMessage = new ExceptionMessage();
            exception.UserDefinedMessage.Details = new List<ExceptionMessage>();

            foreach (var error in errors)
            {
                exception.UserDefinedMessage.Details.Add(new ExceptionMessage
                {
                    Message = error.Description
                });
            }
            exception.UserDefinedMessage.Message = exception.UserDefinedMessage.Details.First().Message;

            return exception;
        }
    }
}