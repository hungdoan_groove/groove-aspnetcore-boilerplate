﻿using ApplicationCore.Core;
using Groove.AspNetCore.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Identity.Entities
{
    /// <summary>
    /// Inherit from IdentityRole, becasue I want to use use/role scheme/management from ASP.NET Core Identity
    /// </summary>
    public class Role : IdentityRole<long>, IVersionedEntity<long>
    {
        public DateTimeOffset CreatedDate { get; set; }
        public long CreatedByUserId { get; set; }     // use to query/join
        public string CreatedByUserName { get; set; } // Use to display

        public DateTimeOffset ModifiedDate { get; set; }
        public long ModifiedByUserId { get; set; }     // use to query/join
        public string ModifiedByUserName { get; set; } // Use to display
        public byte[] RowVersion { get; set; }
    }
}