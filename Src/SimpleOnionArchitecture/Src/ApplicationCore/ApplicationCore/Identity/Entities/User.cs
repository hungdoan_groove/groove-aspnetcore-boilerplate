﻿using ApplicationCore.Core;
using ApplicationCore.Todos.Entities;
using Groove.AspNetCore.Identity;
using Groove.AspNetCore.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Identity.Entities
{
    /// <summary>
    /// Inherit from IdentityRole, becasue I want to use use/role scheme/management from ASP.NET Core Identity
    /// </summary>
    public class User : IdentityUser<long>, IVersionedEntity<long>
    {
        public DateTimeOffset CreatedDate { get; set; }
        public long CreatedByUserId { get; set; }     // use to query/join
        public string CreatedByUserName { get; set; } // Use to display

        public DateTimeOffset UpdatedDate { get; set; }
        public long UpdatedByUserId { get; set; } // use to query/join
        public string UpdatedByUserName { get; set; } // Use to display

        public byte[] RowVersion { get; set; }

        public IEnumerable<Todo> CreatedTodos { get; set; }

        public User CreateBy(UserIdentity<long> issuer)
        {
            var now = DateTimeOffset.UtcNow;

            CreatedByUserId = issuer.Id;
            CreatedByUserName = issuer.UserName;
            CreatedDate = now;

            return this;
        }

        public User UpdateBy(UserIdentity<long> issuer)
        {
            var now = DateTimeOffset.UtcNow;

            UpdatedByUserId = issuer.Id;
            UpdatedByUserName = issuer.UserName;
            UpdatedDate = now;

            return this;
        }
    }
}