﻿using AutoMapper;
using ApplicationCore.Identity.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Identity.Models
{
    public class UserCreateModel
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public string RePassword { get; set; }
    }

    public class UserCreateModelMapper : Profile
    {
        public UserCreateModelMapper()
        {
            CreateMap<UserCreateModel, User>()
                .ForMember(d => d.UserName, opt => opt.MapFrom(s => s.Email));
        }
    }

    public class UserCreateModelValidator : AbstractValidator<UserCreateModel>
    {
        public UserCreateModelValidator()
        {
            RuleFor(p => p.Email).NotEmpty().EmailAddress();
            RuleFor(p => p.Password).NotEmpty();
            RuleFor(p => p.RePassword).NotEmpty();
            RuleFor(p => p.Password).Equal(p => p.RePassword).WithMessage("Password and Re-password are not match");
            RuleFor(p => p.Password).MinimumLength(8);
            RuleFor(p => p.Password).RequireDigit();
            RuleFor(p => p.Password).RequireUppercase();
        }
    }
}