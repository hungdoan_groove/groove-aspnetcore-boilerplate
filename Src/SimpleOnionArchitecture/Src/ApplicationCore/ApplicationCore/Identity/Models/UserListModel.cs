﻿using AutoMapper;
using ApplicationCore.Core;
using ApplicationCore.Identity.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Identity.Models
{
    public class UserListModel : ReadModelBase<long>
    {
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}