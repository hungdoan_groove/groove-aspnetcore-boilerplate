﻿using AutoMapper;
using ApplicationCore.Identity.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Identity.Models
{
    public class UserUpdateModel
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class UserUpdateModelMapper : Profile
    {
        public UserUpdateModelMapper()
        {
            CreateMap<UserUpdateModel, User>();
        }
    }
}