﻿using AutoMapper;
using ApplicationCore.Identity.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Identity.Models
{
    public class UserDetailModel
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
    }
}