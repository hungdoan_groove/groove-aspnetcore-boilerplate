﻿using ApplicationCore.Identity.Entities;
using ApplicationCore.Identity.Models;
using Groove.AspNetCore.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationCore.Identity.Repositories
{
    public interface IUserRepository
    {
        Task<IEnumerable<UserListModel>> GetListAsync(Action<ISpecification<UserListModel>> query);

        Task<UserDetailModel> GetDetailAsync(long id);

        Task<User> GetByUserNameAsync(string userName);
    }
}