﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ApplicationCore.Todos.Models;
using ApplicationCore.Todos.Entities;
using ApplicationCore.Todos.Repositories;
using Groove.AspNetCore.Identity;
using Groove.AspNetCore.UnitOfWork;

namespace ApplicationCore.Todos.Services
{
    public class TodoService : ITodoService
    {
        private readonly IGenericRepository<Todo,long> _genericRepository;
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public TodoService(IMapper mapper, IUnitOfWork uow, IGenericRepository<Todo, long> genericRepository)
        {
            this._uow = uow;
            _genericRepository = genericRepository;
            this._mapper = mapper;
        }

        public async Task<long> CreateAsync(TodoModel model, UserIdentity<long> issuer)
        {
            var entity = this._mapper.Map<Todo>(model);

            entity.CreateBy(issuer).UpdateBy(issuer);

            _genericRepository.Create(entity);

            await _uow.SaveChangesAsync();
            return entity.Id;
        }

        public async Task UpdateAsync(TodoModel model, UserIdentity<long> issuer)
        {
            var entity = _mapper.Map<Todo>(model);

            entity.UpdateBy(issuer);

            _genericRepository.Update(entity);

            await _uow.SaveChangesAsync();
        }
    }
}