﻿using ApplicationCore.Todos.Models;
using ApplicationCore.Todos.Entities;
using Groove.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Todos.Services
{
    public interface ITodoService
    {
        Task<long> CreateAsync(TodoModel model, UserIdentity<long> issuer);

        Task UpdateAsync(TodoModel model, UserIdentity<long> issuer);
    }
}