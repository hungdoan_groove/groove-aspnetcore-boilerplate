﻿using ApplicationCore.Todos.Entities;
using ApplicationCore.Todos.Models;
using Groove.AspNetCore.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationCore.Todos.Repositories
{
    public interface ITodoRepository
    {
        Task<IEnumerable<TodoListModel>> GetListAsync(Action<ISpecification<Todo>> query);

        Task<TodoModel> GetDetailByIdAsync(long id);
    }
}