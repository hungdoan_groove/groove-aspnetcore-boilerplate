﻿using ApplicationCore.Core;
using Groove.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Todos.Entities
{
    public class CheckList : EntityBase<long>
    {

        public string Description { get; set; }

        public Todo Todo { get; set; }
    }
}
