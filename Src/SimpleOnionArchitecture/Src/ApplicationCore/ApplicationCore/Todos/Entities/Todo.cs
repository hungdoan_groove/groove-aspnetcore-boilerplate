﻿using ApplicationCore.Core;
using ApplicationCore.Identity.Entities;
using Groove.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Todos.Entities
{
    public class Todo : EntityBase<long>
    {
        public string Title { get; set; }
        public string Description { get; set; }

        public List<CheckList> CheckLists { get; set; }

        public User CreatedByUser { get; set; }
    }
}