﻿using AutoMapper;
using ApplicationCore.Todos.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Todos.Models
{
    public class CheckListModel
    {
        public string Description { get; set; }
        public long Id { get; set; }
    }

    public class CheckListModelMapper : Profile
    {
        public CheckListModelMapper()
        {
            CreateMap<CheckListModel, CheckList>();
        }
    }
}