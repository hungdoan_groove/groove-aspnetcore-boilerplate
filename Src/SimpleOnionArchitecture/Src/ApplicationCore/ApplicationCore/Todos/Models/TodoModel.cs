﻿using AutoMapper;
using ApplicationCore.Todos.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Todos.Models
{
    /// <summary>
    /// This DTO is used for CREATE/UPDATE/READ operartions
    /// </summary>
    public class TodoModel
    {
        public IEnumerable<CheckListModel> CheckLists { get; set; }
        public string Description { get; set; }
        public long Id { get; set; }
        public string Title { get; set; }
    }

    public class TodoModelValidator : AbstractValidator<TodoModel>
    {
        public TodoModelValidator()
        {
            RuleFor(p => p.Title).NotEmpty();
        }
    }
}