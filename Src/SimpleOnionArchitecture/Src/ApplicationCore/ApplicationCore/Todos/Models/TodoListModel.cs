﻿using AutoMapper;
using ApplicationCore.Todos.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Todos.Models
{
    /// <summary>
    /// This DTO is used for listing
    /// Which provide some importance field only
    /// </summary>
    public class TodoListModel
    {
        public string Description { get; set; }
        public long Id { get; set; }

        public string Title { get; set; }
    }
}