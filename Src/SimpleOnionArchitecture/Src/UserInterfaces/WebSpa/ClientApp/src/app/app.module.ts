import { BrowserModule } from '@angular/platform-browser';
import { NgModule, FactoryProvider, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceRegistryService, LocalStorageService, AuthHttpService } from 'src/app/shared';
import { LayoutComponent } from './layout/layout.component';
import { PageNotFoundComponent } from './error/page-not-found/page-not-found.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HeaderComponent } from './layout/header/header.component';
import { NavigationComponent } from './layout/navigation/navigation.component';
import { APP_CONSTRAIN } from 'src/app/app-constrain'
import { NotificationService } from 'src/app/shared/components/dialog/notification.service';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { FormValidationService } from 'src/app/shared/components/form';
import { ErrorModule } from './error/error.module';
const APP_INITIALIZER_PROVIDER: FactoryProvider = {
  provide: APP_INITIALIZER,
  useFactory: (ServiceRegistryService: ServiceRegistryService) => {

    // Do initing of services that is required before app loads
    // NOTE: this factory needs to return a function (that then returns a promise)
    return () => ServiceRegistryService.load('/configuration/serviceRegistry').toPromise();
  },
  deps: [ServiceRegistryService],
  multi: true
};

export function tokenGetter() {
  return localStorage.getItem(APP_CONSTRAIN.authentication.tokenStoreKey);
}

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    FooterComponent,
    HeaderComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ErrorModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['localhost:64478']
      }
    })
  ],
  providers: [
    LocalStorageService,
    ServiceRegistryService,
    NotificationService,
    AuthHttpService,
    AuthenticationService,
    FormValidationService,
    APP_INITIALIZER_PROVIDER // Use FactoryProvider to wait for app to load configuration file before starting the app.
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
