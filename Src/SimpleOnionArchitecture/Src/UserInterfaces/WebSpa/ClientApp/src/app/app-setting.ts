// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const APP_SETTINGS = {
  clientId: window['configurations'].clientId,
  appVersion: window['configurations'].appVersion
};
