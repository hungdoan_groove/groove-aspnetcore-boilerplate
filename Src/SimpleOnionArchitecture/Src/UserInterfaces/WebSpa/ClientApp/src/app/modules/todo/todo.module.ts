import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoListComponent } from './pages/todo-list/todo-list.component';
import { TodoFormComponent } from './pages/todo-form/todo-form.component';
import { TodoDetailComponent } from './pages/todo-detail/todo-detail.component';
import { TodoRoutingModule } from './todo-routing.module';
import { ErrorModule } from '../../error/error.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ErrorModule,
    TodoRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [TodoListComponent, TodoFormComponent, TodoDetailComponent]
})
export class TodoModule { }
