import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError, finalize } from 'rxjs/operators';
import { ServiceRegistryService } from 'src/app/shared';
import { NotificationService } from 'src/app/shared/components/dialog/notification.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(
    private _http: HttpClient,
    private _serviceRegistry: ServiceRegistryService,
    private _notificationService: NotificationService
  ) { }


  public signin(userName: string, password: string): Observable<any> {
    var headers = new HttpHeaders({ 'Content-type': 'application/json' });
    let body = JSON.stringify({
      userName: userName,
      password: password
    });

    return this._http.post(`${this._serviceRegistry.registry.apiUrl}/api/authentication/token`, body, {
      headers: headers
    });
  }
}
