import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from 'src/app/layout/layout.component';
import { PageNotFoundComponent } from 'src/app/error/page-not-found/page-not-found.component';

const routes: Routes = [
  { path: 'account', loadChildren: './modules/account/account.module#AccountModule' },
  {
    path: '', component: LayoutComponent, children: [
      { path: 'user', loadChildren: './modules/identity/user/user.module#UserModule' },
      { path: 'todo', loadChildren: './modules/todo/todo.module#TodoModule' }
    ]
  },
  {
    path: '**', component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
