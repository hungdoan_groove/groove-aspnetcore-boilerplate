import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserFormComponent } from 'src/app/modules/identity/user/pages/user-form/user-form.component';
import { UserListComponent } from 'src/app/modules/identity/user/pages/user-list/user-list.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule
  ],
  declarations: [
    UserListComponent,
    UserFormComponent]
})
export class UserModule { }
