import { Injectable } from '@angular/core';
import { ICreateFormService, IViewFormService, IUpdateFormService } from 'src/app/shared/components/form';
import { Observable } from 'rxjs';
import { AuthHttpService } from 'src/app/shared';

@Injectable({
  providedIn: 'root'
})
export class UserService implements ICreateFormService, IViewFormService, IUpdateFormService {


  edit(id: any, formData: any): Observable<any> {
    return this._apiHttp.put(`/api/user/${id}`, formData);
  }

  getFormData(id: any): Observable<any> {
    return this._apiHttp.get(`/api/user/${id}`);
  }

  create(formData: any): Observable<any> {
    return this._apiHttp.post(`/api/user`, formData);
  }

  getViewFormUrl(id: any): string {
    return `/user/form/view/${id}`;
  }
  getListPageUrl(): string {
    return `/user/list`;
  }

  constructor(private _apiHttp: AuthHttpService) {

  }
}
