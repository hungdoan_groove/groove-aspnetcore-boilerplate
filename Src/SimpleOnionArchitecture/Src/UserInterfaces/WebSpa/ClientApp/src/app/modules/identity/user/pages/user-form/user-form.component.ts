import { Component, OnInit } from '@angular/core';
import { FormBaseComponent, FormValidationService } from 'src/app/shared/components/form';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'src/app/shared/components/dialog/notification.service';
import { UserService } from 'src/app/modules/identity/user/services/user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent extends FormBaseComponent implements OnInit {
  constructor(protected route: ActivatedRoute,
    protected router: Router,
    protected userService: UserService,
    protected notificationService: NotificationService,
    protected validationService: FormValidationService,
    private _notificationService: NotificationService,
  ) {
    super(route, router, notificationService, userService, validationService);
    super.formOnInit("User", {});
  }

  ngOnInit() {
  }

}
