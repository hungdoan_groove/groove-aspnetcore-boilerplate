import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TodoService } from '../../services/todo.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TodoDto } from '../../models/todo-dto';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.scss']
})
export class TodoFormComponent implements OnInit {
  public isCreateMode: boolean = false;
  public isUpdateMode: boolean = false;
  public id: string;
  public formGroup: FormGroup;

  private _formMode: string;


  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _fb: FormBuilder,
    private _todoService: TodoService
  ) {

    this.formGroup = this._fb.group({
      title: ['', Validators.required],
      description: ['', Validators.maxLength(300)]
    });

    this._formMode = _route.snapshot.data["formMode"];

    if (this._formMode == "create") {
      this.isCreateMode = true;
    }

    if (this._formMode == "update") {
      this.isUpdateMode = true;
    }

    this._route.params.subscribe((params: Params) => {
      this.id = params['id'];

      this.initForm();
    });
  }

  private setFormData(data: TodoDto) {
    Object.keys(data).forEach(name => {
      if (this.formGroup.controls[name]) {
        this.formGroup.controls[name].setValue(data[name]);
      }
    });
  }

  private initForm() {
    if (this.isUpdateMode) {
      this._todoService.getDetailData(this.id).subscribe(result => {
        this.setFormData(result);
      });
    }
  }

  ngOnInit() {
  }

}
