import { Injectable } from '@angular/core';
import { APP_CONSTRAIN } from 'src/app/app-constrain';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private _userTokenStoreKey: string = ".access_token";

  constructor() { }

  public storeToken(token: string) {
    localStorage.setItem(APP_CONSTRAIN.authentication.tokenStoreKey, token);
  }

}
