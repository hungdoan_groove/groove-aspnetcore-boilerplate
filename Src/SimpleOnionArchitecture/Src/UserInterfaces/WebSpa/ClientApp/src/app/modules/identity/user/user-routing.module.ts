import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from 'src/app/modules/identity/user/pages/user-list/user-list.component';
import { UserFormComponent } from 'src/app/modules/identity/user/pages/user-form/user-form.component';

const routes: Routes = [
  { path: 'list', component: UserListComponent },
  { path: 'form/:mode/:id', component: UserFormComponent }, // View / Edit
  { path: 'form/:mode', component: UserFormComponent }      // Create
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
