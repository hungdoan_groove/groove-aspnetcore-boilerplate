export class TodoDto {
  public title: string;
  public description: string;
}
