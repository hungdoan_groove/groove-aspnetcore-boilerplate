import { Injectable } from '@angular/core';
import { ICreateFormService, IViewFormService, IUpdateFormService } from 'src/app/shared/components/form';
import { Observable } from 'rxjs';
import { AuthHttpService } from 'src/app/shared';
import { TodoDto } from '../models/todo-dto';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  getDetailData(id: any): Observable<TodoDto> {
    return this._apiHttp.get(`/api/todo/${id}`);
  }

  constructor(private _apiHttp: AuthHttpService) {

  }
}
