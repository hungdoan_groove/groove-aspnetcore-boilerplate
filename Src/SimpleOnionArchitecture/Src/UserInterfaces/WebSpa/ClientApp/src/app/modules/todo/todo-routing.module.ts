import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoListComponent } from './pages/todo-list/todo-list.component';
import { TodoDetailComponent } from './pages/todo-detail/todo-detail.component';
import { TodoFormComponent } from './pages/todo-form/todo-form.component';
import { PageNotFoundComponent } from '../../error/page-not-found/page-not-found.component';

const routes: Routes = [
  { path: 'list', component: TodoListComponent },
  { path: 'create', data: { formMode: 'create' }, component: TodoFormComponent },
  { path: 'update/:id', data: { formMode: 'update' }, component: TodoFormComponent },
  { path: 'detail/:id', component: TodoDetailComponent },
  {
    path: '**', component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TodoRoutingModule { }
