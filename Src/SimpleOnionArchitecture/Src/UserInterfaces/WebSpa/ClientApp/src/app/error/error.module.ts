import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { PageNotFoundComponent } from 'src/app/error/page-not-found/page-not-found.component';




@NgModule({
  imports: [

  ],
  declarations: [PageNotFoundComponent]
})
export class ErrorModule {
  static ERROR_FALLBACK_ROUTES = [
    {
      path: '**', component: PageNotFoundComponent
    }
  ];


}
