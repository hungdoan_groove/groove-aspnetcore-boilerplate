import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { ServiceRegistryService } from 'src/app/shared';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from 'src/app/shared/components/dialog/notification.service';
import { AccountService } from 'src/app/modules/account/services/account.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public returnUrl: string;
  public formData: any = {};
  public message: string = null;
  public isError: boolean = false;
  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _authService: AuthenticationService,
    private _accountService: AccountService,
    private _notificationService: NotificationService
  ) { }

  ngOnInit() {
    // get return url from route parameters or default to '/'
    this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';
  }


  public signin() {
    this._accountService.signin(this.formData.userName, this.formData.password).subscribe(
      data => {
        this.isError = false;
        this._authService.storeToken(data.access_token);
        this._router.navigate([this.returnUrl]);
      },
      error => {
        this.isError = true;

        let httpError: HttpErrorResponse = error;
        if (httpError.status === 400) {

          this.message = httpError.error.message;
        } else {
          this._notificationService.prompError(httpError.message);
        }
      });
  }
}
