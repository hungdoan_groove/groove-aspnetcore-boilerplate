﻿using ApplicationCore.Todos.Entities;
using ApplicationCore.Todos.Models;
using ApplicationCore.Todos.Repositories;
using ApplicationCore.Todos.Services;
using Groove.AspNetCore.Identity;
using Groove.AspNetCore.Mvc;
using Groove.AspNetCore.Mvc.ActionResults;
using Groove.AspNetCore.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebApi.Controllers.Todos
{
    [Route("api/todo")]
    public class TodoController : BaseController
    {
        private readonly ITodoService _todoService;
        private readonly ITodoRepository _todoRepository;
        private readonly IRequestIdentityService _requestIdentityService;

        public TodoController(ITodoService todoService, ITodoRepository todoRepository, IRequestIdentityService requestIdentityService)
        {
            _todoService = todoService;
            _todoRepository = todoRepository;
            _requestIdentityService = requestIdentityService;
        }

        [Route("")]
        [HttpPost]
        public async Task<IActionResult> Create(TodoModel model)
        {
            if (ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var issuerIdentity = _requestIdentityService.GetCurrentIdentity<long>();
            var result = await _todoService.CreateAsync(model, issuerIdentity);
            return Ok(new ValueDto(result));
        }

        [Route("")]
        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            return Ok(await _todoRepository.GetListAsync(query =>
            {
                query.OrderBy(p => p.CreatedDate, OrderByDirection.Decending);
            }));
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<IActionResult> GetById(long id)
        {
            var result = await _todoRepository.GetDetailByIdAsync(id);
            return Ok(result);
        }

        [Route("{id}")]
        [HttpPut]
        public async Task<IActionResult> UpdateAsync(long id, TodoModel model)
        {
            var issuerIdentity = _requestIdentityService.GetCurrentIdentity<long>();
            await _todoService.UpdateAsync(model, issuerIdentity);
            return Ok();
        }
    }
}