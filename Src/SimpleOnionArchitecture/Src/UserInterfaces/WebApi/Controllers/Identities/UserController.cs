﻿using ApplicationCore.Identity.Entities;
using ApplicationCore.Identity.Models;
using ApplicationCore.Identity.Repositories;
using ApplicationCore.Identity.Services;
using Groove.AspNetCore.Identity;
using Groove.AspNetCore.Mvc;
using Groove.AspNetCore.Mvc.ActionResults;
using Groove.AspNetCore.UnitOfWork;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Controllers.Identities
{
    [Route("api/user")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IUserRepository _userRepository;
        private readonly IRequestIdentityService _requestIdentityService;

        public UserController(IUserService userService, IUserRepository userRepository, IRequestIdentityService requestIdentityService)
        {
            _userService = userService;
            _userRepository = userRepository;
            _requestIdentityService = requestIdentityService;
        }

        [Route("")]
        [HttpGet]
        public async Task<IActionResult> GetAllUser()
        {
            var result = await _userRepository.GetListAsync(query=>
            {
                query.OrderBy(p=>p.CreatedDate, OrderByDirection.Decending);
            });

            return Ok(result);
        }

        [Route("")]
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CreateUser([FromBody]UserCreateModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var issuerIdentity = _requestIdentityService.GetCurrentIdentity<long>();
            var userId =  await _userService.CreateUserAsync(model, issuerIdentity);

            return Ok(new ValueDto(userId));
        }

        [Route("{id}")]
        [HttpPut]
        [Authorize]
        public async Task<IActionResult> UpdateUser(long id, [FromBody]UserUpdateModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var issuerIdentity = _requestIdentityService.GetCurrentIdentity<long>();
            await _userService.UpdateUserAsync(id, model, issuerIdentity);

            return Ok();
        }

        [Route("{id}")]
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetUser(long id)
        {
            var result = await _userRepository.GetDetailAsync(id);
            return Ok(result);
        }
    }
}