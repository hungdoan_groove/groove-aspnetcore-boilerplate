﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Infrastructures.DbMigration.Migrations
{
    public partial class updateTodoRelationShip : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Todo_CreatedByUserId",
                table: "Todo",
                column: "CreatedByUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Todo_AspNetUsers_CreatedByUserId",
                table: "Todo",
                column: "CreatedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Todo_AspNetUsers_CreatedByUserId",
                table: "Todo");

            migrationBuilder.DropIndex(
                name: "IX_Todo_CreatedByUserId",
                table: "Todo");
        }
    }
}
