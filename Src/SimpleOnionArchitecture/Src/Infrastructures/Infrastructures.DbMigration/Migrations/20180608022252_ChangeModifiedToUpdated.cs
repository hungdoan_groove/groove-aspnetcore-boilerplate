﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Infrastructures.DbMigration.Migrations
{
    public partial class ChangeModifiedToUpdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ModifiedDate",
                table: "Todo",
                newName: "UpdatedDate");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserName",
                table: "Todo",
                newName: "UpdatedByUserName");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "Todo",
                newName: "UpdatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedDate",
                table: "CheckList",
                newName: "UpdatedDate");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserName",
                table: "CheckList",
                newName: "UpdatedByUserName");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "CheckList",
                newName: "UpdatedByUserId");

            migrationBuilder.RenameColumn(
                name: "ModifiedDate",
                table: "AspNetUsers",
                newName: "UpdatedDate");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserName",
                table: "AspNetUsers",
                newName: "UpdatedByUserName");

            migrationBuilder.RenameColumn(
                name: "ModifiedByUserId",
                table: "AspNetUsers",
                newName: "UpdatedByUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UpdatedDate",
                table: "Todo",
                newName: "ModifiedDate");

            migrationBuilder.RenameColumn(
                name: "UpdatedByUserName",
                table: "Todo",
                newName: "ModifiedByUserName");

            migrationBuilder.RenameColumn(
                name: "UpdatedByUserId",
                table: "Todo",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "UpdatedDate",
                table: "CheckList",
                newName: "ModifiedDate");

            migrationBuilder.RenameColumn(
                name: "UpdatedByUserName",
                table: "CheckList",
                newName: "ModifiedByUserName");

            migrationBuilder.RenameColumn(
                name: "UpdatedByUserId",
                table: "CheckList",
                newName: "ModifiedByUserId");

            migrationBuilder.RenameColumn(
                name: "UpdatedDate",
                table: "AspNetUsers",
                newName: "ModifiedDate");

            migrationBuilder.RenameColumn(
                name: "UpdatedByUserName",
                table: "AspNetUsers",
                newName: "ModifiedByUserName");

            migrationBuilder.RenameColumn(
                name: "UpdatedByUserId",
                table: "AspNetUsers",
                newName: "ModifiedByUserId");
        }
    }
}
