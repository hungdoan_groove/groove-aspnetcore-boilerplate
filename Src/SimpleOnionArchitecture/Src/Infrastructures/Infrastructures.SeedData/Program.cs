﻿using ApplicationCore.Identity.Entities;
using ApplicationCore.Todos.Entities;
using Groove.AspNetCore.Identity;
using Infrastructures.SeedData;
using Infratructures;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Infratructure.SeedData
{
    internal class Program
    {
        private static IServiceProvider _serviceProvider;

        private static void Main(string[] args)
        {
            Console.WriteLine("Starting to seed data");

            _serviceProvider = ConfigureService(new ServiceCollection(), args);

            using (var dbContext = _serviceProvider.GetService<ApplicationDbContext>())
            {
                using (var transaction = dbContext.Database.BeginTransaction())
                {
                    SeedDataAsync(dbContext).Wait();
                    transaction.Commit();

                    Console.WriteLine("Commit all seed");
                }
            }

            Console.WriteLine("Seed data successful");
        }

        private static IServiceProvider ConfigureService(IServiceCollection services, string[] args)
        {
            var dbContextFactory = new DesignTimeDbContextFactory();

            services.AddLogging();
            services.AddScoped<ApplicationDbContext>(p => dbContextFactory.CreateDbContext(args));

            services.AddIdentity<User, Role>()
                            .AddEntityFrameworkStores<ApplicationDbContext>()
                            .AddDefaultTokenProviders(); // protection provider responsible for generating an email confirmation token or a password reset token
            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = false;

                // User settings
                options.User.RequireUniqueEmail = true;
            });

            // Build the IoC from the service collection
            return services.BuildServiceProvider();
        }

        private static async Task SeedDataAsync(ApplicationDbContext dbContext)
        {
            await SeedUserDataAsync(dbContext);
            await SeedTodoDataAsync(dbContext);
        }

        private static async Task SeedUserDataAsync(ApplicationDbContext dbContext)
        {
            if (!await dbContext.Set<User>().AnyAsync())
            {
                Console.WriteLine("Start to seed user info");

                var userManagement = _serviceProvider.GetService<UserManager<User>>();
                var user = new User
                {
                    UserName = "system",
                    Email = "hung.doan@groovetechnology.com",
                    CreatedByUserId = 1,
                    CreatedDate = DateTimeOffset.UtcNow,
                    CreatedByUserName = "system",
                    UpdatedByUserId = 1,
                    UpdatedDate = DateTimeOffset.UtcNow,
                    UpdatedByUserName = "system"
                };
                await userManagement.CreateAsync(user, "Password@1");

                Console.WriteLine("Finish seed user info");
            }
        }

        private static async Task SeedTodoDataAsync(ApplicationDbContext dbContext)
        {
            var todoBbSet = dbContext.Set<Todo>();
            var userDbSet = dbContext.Set<User>();
            if (!await todoBbSet.AnyAsync())
            {
                Console.WriteLine("Start to seed Todo info");

                var firstUser = await userDbSet.FirstAsync();
                var userIdentity = new UserIdentity<long>
                {
                    Id = firstUser.Id,
                    UserName = firstUser.UserName
                };
                var todo = new Todo
                {
                    Title = "Sample todo",
                    Description =  "Dodo description",
                };
                todo.CreateBy(userIdentity).UpdateBy(userIdentity);

                todoBbSet.Add(todo);

                await dbContext.SaveChangesAsync();
                Console.WriteLine("Finish seed Todo info");
            }
        }
    }
}