﻿using ApplicationCore.Identity.Entities;
using ApplicationCore.Todos.Entities;
using Groove.AspNetCore.EntityFramework;
using Infrastructures.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.EntityConfigurations
{
    public class CheckListConfig : EntityConfigurationBase<CheckList, long>
    {
        public override void OnConfigure(EntityTypeBuilder<CheckList> builder)
        {

        }
    }
}
