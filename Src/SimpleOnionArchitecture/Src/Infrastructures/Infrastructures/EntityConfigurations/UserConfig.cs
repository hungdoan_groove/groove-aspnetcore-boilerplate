﻿using ApplicationCore.Identity.Entities;
using Groove.AspNetCore.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.EntityConfigurations
{
    public class UserConfig : EntityConfiguration<User>
    {
        public override void OnConfigure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.RowVersion).IsConcurrencyToken();

            builder.Property(p => p.CreatedByUserName).IsRequired();
            builder.Property(p => p.CreatedDate).IsRequired();

            builder.Property(p => p.UpdatedByUserName).IsRequired();
            builder.Property(p => p.UpdatedDate).IsRequired();

        }
    }
}
