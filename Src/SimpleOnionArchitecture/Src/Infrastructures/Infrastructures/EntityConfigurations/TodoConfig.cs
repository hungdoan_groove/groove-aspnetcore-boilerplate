﻿using ApplicationCore.Identity.Entities;
using ApplicationCore.Todos.Entities;
using Groove.AspNetCore.EntityFramework;
using Infrastructures.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.EntityConfigurations
{
    public class TodoConfig : EntityConfigurationBase<Todo, long>
    {
        public override void OnConfigure(EntityTypeBuilder<Todo> builder)
        {
            builder.HasOne(p => p.CreatedByUser).WithMany(p => p.CreatedTodos).HasForeignKey(p => p.CreatedByUserId);
        }
    }
}
