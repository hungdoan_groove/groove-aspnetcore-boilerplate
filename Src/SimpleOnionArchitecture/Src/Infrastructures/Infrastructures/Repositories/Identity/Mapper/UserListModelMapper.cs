﻿using AutoMapper;
using ApplicationCore.Identity.Entities;
using ApplicationCore.Identity.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructures.Repositories.Identity.Mapper
{
    public class UserListModelMapper : Profile
    {
        public UserListModelMapper()
        {
            CreateMap<User, UserListModel>();
        }
    }
}