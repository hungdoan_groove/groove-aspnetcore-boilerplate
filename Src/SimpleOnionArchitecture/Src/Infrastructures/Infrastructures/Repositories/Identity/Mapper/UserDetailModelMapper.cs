﻿using AutoMapper;
using ApplicationCore.Identity.Entities;
using ApplicationCore.Identity.Models;

namespace Infrastructures.Repositories.Identity.Mapper
{
    public class UserDetailModelMapper : Profile
    {
        public UserDetailModelMapper()
        {
            CreateMap<User, UserDetailModel>();
        }
    }
}