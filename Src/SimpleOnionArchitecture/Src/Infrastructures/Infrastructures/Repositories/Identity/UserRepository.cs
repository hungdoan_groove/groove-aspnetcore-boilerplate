﻿using AutoMapper;
using ApplicationCore.Identity.Entities;
using ApplicationCore.Identity.Models;
using ApplicationCore.Identity.Repositories;
using Groove.AspNetCore.UnitOfWork;
using Groove.AspNetCore.UnitOfWork.EntityFramework;
using Infratructures;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructures.Repositories.Identity
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        public UserRepository(
            ApplicationDbContext dbContext,
            UserManager<User> userManager,
            IMapper mapper)
        {
            _userManager = userManager;
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<User> GetByUserNameAsync(string userName)
        {
            return await _userManager.FindByNameAsync(userName);
        }

        public async Task<UserDetailModel> GetDetailAsync(long id)
        {
            return await _dbContext.Set<User>().MapQueryTo<UserDetailModel>(_mapper).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<UserListModel>> GetListAsync(Action<ISpecification<UserListModel>> query)
        {
            return await _dbContext.Set<User>().MapQueryTo<UserListModel>(_mapper).ApplySpecification(query).ToListAsync();
        }
    }
}