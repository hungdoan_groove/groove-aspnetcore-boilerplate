﻿using AutoMapper;
using ApplicationCore.Todos.Models;
using ApplicationCore.Todos.Entities;
using ApplicationCore.Todos.Repositories;
using Groove.AspNetCore.UnitOfWork.EntityFramework;
using Infratructures;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using Groove.AspNetCore.UnitOfWork;
using ApplicationCore.Identity.Models;

namespace Infrastructures.Repositories.Todos
{
    public class TodoRepository : ITodoRepository
    {
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _dbContext;

        public TodoRepository(
            ApplicationDbContext dbContext,
            IMapper mapper)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }

        public async Task<TodoModel> GetDetailByIdAsync(long id)
        {
            return await _dbContext.Set<Todo>().Include(p => p.CheckLists).Where(p => p.Id == id).MapQueryTo<TodoModel>(_mapper).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<TodoListModel>> GetListAsync(Action<ISpecification<Todo>> query)
        {
            return await this._dbContext.Set<Todo>().ApplySpecification(query).MapQueryTo<TodoListModel>(_mapper).ToListAsync();
        }
    }
}