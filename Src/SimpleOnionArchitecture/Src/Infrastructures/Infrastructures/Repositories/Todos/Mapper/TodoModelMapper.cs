﻿using AutoMapper;
using ApplicationCore.Todos.Entities;
using ApplicationCore.Todos.Models;

namespace Infrastructures.Repositories.Todos.Mapper
{
    public class TodoModelMapper : Profile
    {
        public TodoModelMapper()
        {
            CreateMap<TodoModel, Todo>();
        }
    }
}