﻿using AutoMapper;
using ApplicationCore.Todos.Entities;
using ApplicationCore.Todos.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructures.Repositories.Todos.Mapper
{
    public class TodoListModelMapper : Profile
    {
        public TodoListModelMapper()
        {
            CreateMap<Todo, TodoListModel>();
        }
    }
}