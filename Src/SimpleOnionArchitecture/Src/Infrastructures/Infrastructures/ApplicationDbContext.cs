﻿using ApplicationCore.Identity.Entities;
using Groove.AspNetCore.EntityFramework;
using Groove.AspNetCore.UnitOfWork;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Infratructures
{
    /// <summary>
    /// In normal case. We would need to inherit to DbContext only.
    /// But, in this case. We are using Microsoft Identity to manage user/roles...
    /// And this Microsoft Identity has it own Entities (user, role, claims,...)
    /// So that, I have to inherit from IdentityDbContext to be able to access these entities.
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<User, Role, long>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            var configLoader = new ConfigurationLoader<ApplicationDbContext>(builder);
            configLoader.Load();
        }
    }
}