<!-- TOC -->

- [1. Onion Architecture](#1-onion-architecture)
    - [1.1. Layered Architecture](#11-layered-architecture)
    - [1.2. Onion architecture](#12-onion-architecture)
        - [1.2.1. Introduction](#121-introduction)
        - [1.2.2. Layers](#122-layers)
    - [1.3. Implementation](#13-implementation)
        - [1.3.1. Project Structure](#131-project-structure)
        - [1.3.2. Getting started](#132-getting-started)
        - [1.3.3. Basic development flow](#133-basic-development-flow)
            - [1.3.3.1. Data flow](#1331-data-flow)
            - [1.3.3.2. Steps:](#1332-steps)
        - [1.3.4. Database Migration](#134-database-migration)
            - [1.3.4.1. Installation](#1341-installation)
            - [1.3.4.2. Add Migration](#1342-add-migration)
            - [1.3.4.3. Update Database](#1343-update-database)
            - [1.3.4.4. Seed Data](#1344-seed-data)
        - [1.3.5. Angular-cli](#135-angular-cli)
        - [1.3.6. Design Pattern](#136-design-pattern)
            - [1.3.6.1. Repository](#1361-repository)
            - [1.3.6.2. Unit of Work](#1362-unit-of-work)
        - [1.3.7. NOTES](#137-notes)
        - [1.3.8. Working with angular form](#138-working-with-angular-form)
            - [1.3.8.1. Generic form](#1381-generic-form)
    - [1.4. References:](#14-references)

<!-- /TOC -->

# 1. Onion Architecture

## 1.1. Layered Architecture

In layered architecture, all the dependencies go in one direction, from presentation to infrastructure.

![Layered architecture][layered-architecture-diagram]

The advantages of Layered Architecture are: 

- It's simple and easy to understand
- It guarantee separation of concerns

But, there are some disadvantages: 

- Low cohesion: Project is is organized around separating technical concerns, It doesn't tend to present business use cases.
- This violates the Dependency Inversion Principle (DIP) because your Domain Layer depend on the  Data Access Layer (Infrastructure Layer). Data access techniques change frequently, we should not depend on that layer.
- Outer layer cannot call inner layers directly.
- Domain Layer depend on Infrastructure Layer. But you know that, Infrastructure layer cannot getting data without knowledge about domain, infrastructure must know which data should provide Domain Layer?. Logically, infrastructure also depend on Domain Layer. Is that weird ?

The Onion Architecture was coined by Jeffrey Palermo in 2008 (<https://jeffreypalermo.com/2008/07/the-onion-architecture-part-1/>)

In Onion Architecture, Database is not the center. It is treated as a external service.In this architecture, Dependency Inversion principle is used heavily to decoupling our application from infrastructure.


Onion Architecture is closely connected to two other architectural styles – Layered and Hexagonal. Similarly to the Layered approach, Onion Architecture uses the concept of layers, but they are a little different

## 1.2. Onion architecture

### 1.2.1. Introduction

![Onion Architecture Diagram][onion-architecture-diagram]

Like the ideal of Ports & Adapters Architecture (Hexagonal) about the direction of dependencies:

- Outer layers: depend on inner layers;
- Inner layers do not know about outer layers.

Key tenets of Onion Architecture:

- The application is built around an independent object model
- Inner layers define interfaces.  Outer layers implement interfaces
- Direction of coupling is toward the center
- All application core code can be compiled and run separate from infrastructure

### 1.2.2. Layers

Layer can be logical layer or physical layer (an dll/assembly in C# as an example). In this documentation, I'm going to taking about physical layer (each layer is mapped to one project in .NET Solution)

Layers in Onion Architecture is vary. But depend on your purpose it would be less or more layers. For example, in CRUD based application there is no Domain Service & Domain model actually, It just about database entity and CRUD operation.

In this project, we are going to implement CRUD based application. Bellow are the layers:

- Inner application layer: How your application organized 

  - **ApplicationCore Layer**: which contain application logic & business logic

- Outer application layer: 

  - **User Interface Layer**: which provide interfaces/API to the world (send-user, third parties,..), so that users can access your application (Web Page, Web API, Web Services,..)
  - **Infrastructure Layer**: which provide interfaces/API to the inner layer to access services from outer world (Database, File System, Mailer Services,...)

In the more complex project which implement Domain Driven Design. The ApplicationCore Layer can be split into Application Layer and Domain Model Layer.


## 1.3. Implementation

### 1.3.1. Project Structure

- **BuildingBlocks** : Includes building block for the application. You can treat this building block as a Core of your application, which should include non-business & re-useable code (common function/utilities,..)

- **UserInterfaces** contains applications used by external consumers to talk to your system (Web API, Web Service, Web App,...)
    - WebApi: Web API
    - WebSpa: Host single page application

- **ApplicationCore** : Which includes your business logic, which contains:
    - Entity Models
    - Repositories contracts/interfaces
    - Business Services

- **Infrastructures** : Used to abstract technical concerns, which contains: 
    - Persistence data infrastructure
            - Repository implementation
            - ...
    - Data access
            - Entity Framework / ORM
            - SQL/No-SQL Query
            - ...

    - Infrastructure implementation used from the `Application Layer`
            - Dependency Injection
            - Logging
            - Cryptography
            - Message Queue
            - File Transfer
            - ...

- **Infrastructures.SeedData** : Seed default data for your infrastructure resource (default users in your databases,..)

End-users/consumers will interact with your `Applications Layer`, This `Applications Layer` will typically use both `Domain Layer` and `Infrastructure Layer` to deal with requests.

In practice, `Domain Layer` should contains business logic only. So that, It should not contains CRUD operations. Your CRUD operations should be handled by `Applications Layer` (CRUD are not business logic, CRUD is to provide entities).

But, We are not doing DDD (Domain Driven Design), we can consider to put our CRUD operations in `Domain Layer` to centralize all user actions. In this situation, we can consider `Applications Layer` as a proxy/presentation layer. Which received requests from clients, do some basic validation and then forward them to `Domain Layer`.

### 1.3.2. Getting started

Step 1: Run Database Migration to generate an database

```bash
#!/bin/bash
$ cd .\UserInterfaces\WebApi

$ dotnet ef database update
```

Step 2: Seed default data

```bash
#!/bin/bash
$ dotnet run --project .\Infrastructures\Infrastructures.SeedData "<put your database connection string here>"
```

Step 3: Build your angular app

```bash
#!/bin/bash
$ cd .\UserInterfaces\WebSpa

$ ng build --watch
```

Step 4: Run your application

### 1.3.3. Basic development flow

> Example: Develop `Create User Api`. When user send request to POST `/api/user`. It will create a new user in your system.

#### 1.3.3.1. Data flow 

```
+----------+ POST /api/user    +------------+                    +--------------+                      +----------+               +--------+
|          +------------------>+            | UserService.Create |              |                      |          |               |        |
|          |                   |            +------------------> |              | UserRepository.Create|          |               |        |
|          |                   |            |                    |              +--------------------> |          |INSERT INTO... |        |
|          |                   |            |                    |              |                      |          +-------------> |        |
|   GUI    |                   |   Web API  |                    |App Service   |                      |Repository|               |Database|
|          |                   |            |                    |              |                      |          | <-------------+        |
|          |                   |            |                    |              | <--------------------+          |               |        |
|          |                   |            | <------------------+              |                      |          |               |        |
|          | <-----------------+            |                    |              |                      |          |               |        |
+----------+                   +------------+                    +--------------+                      +----------+               +--------+

```


#### 1.3.3.2. Steps:

Step 1: Create `User.cs` in `ApplicationCore/[DomainGroup]/Entities` (say ApplicationCore/Identities/Entities) to create an entity for user

- This class should implement interface IEntity or IEntity\<TKey\> (which contain private key):

  ```C#
  public class User : IEntity<long>
  {
    public long Id {get;set;}
  }
  ```
  
  In real-life practice use can create an EntityBase & inherit from it:

    ```C#
    public class EntityBase<TKey> : IEntity<TKey>
    {
        public TKey Id { get; set; }
        public byte[] RowVersion { get; set; }
    }

    public class User : EntityBase<long>
    {
        public long Id {get;set;}
    }
    ```

Step 2: Create `UserConfig.cs` in `Infrastructures/EntityConfigurations/[DomainGroup]` (say Infrastructures/EntityConfigurations/Entities) to configure database mapping, private key, Index, constraints,.. 

- This class should implement IEntityConfiguration interface. This interface will be used as a marker, which is be used in DbContext creation to scan all configuration class in your assembly (take a look at ApplicationDbContext for more detail) :

    ```C#
    public class UserConfig : IEntityConfiguration
    {
      public void Configure(ModelBuilder builder)
      {
          var typeBuilder = builder.Entity<User>();
          typeBuilder.HasKey(p => p.Id);
          typeBuilder.Property(p => p.UserName).HasMaxLength(300);
      }
    }

    ```

- You can inherit from EntityConfiguration<TEntity> to simplified your code:
    ```C#
    public class UserConfig : EntityConfiguration<User> 
    {
      public override void OnConfigure(EntityTypeBuilder<User> builder)
      {        
          builder.HasKey(p => p.Id);
          builder.Property(p => p.UserName).HasMaxLength(300);        
      }
    }
    ```

Step 3: Create `IUserRepository.cs` in `ApplicationCore/[DomainGroup]/Repositories` (say ApplicationCore/[DomainGroup]/Repositories/Identities/IUserRepository.cs) to create IUserRepository, which will be used as an abstraction data access object for your domain, which will support Domain services to query / modify data in in your persistence storage

- This Repository should contain data access functions only. It should not contains any business logic
- This Repository should work with Domain object only. It should not work with DTO (Data Transfer Object) in your Application Layer

Step 4: Create `UserRepository.cs` in `Infrastructures/Repositories/[DomainGroup]` (say Infrastructures/Repositories/Identities/UserRepository.cs) to implement IUserRepository, which was defined in Business Domain Layer

- This Repository will use any technique (DAO, Entity Framework, ADO .NET, SOAP,..) to access your persistence data stores.

Step 5: Create `IUserService.cs` in `ApplicationCore/[DomainGroup]/Services` (say ApplicationCore/Identities/Services/IUserService.cs) to define IUserService, which will be used as business service in your domain.

Step 6: Create `UserService.cs` in `ApplicationCore/[DomainGroup]/Services` (say ApplicationCore/Identities/Services/UserService.cs) to implement IUserService, which was defined in Business Domain Layer.

- This service will use IUserRepository (or others repositories ) to interact with your data store.

Step 7: Create your `UserController.cs` in `UserInterfaces\WebApi\Controllers\[Group]` (say UserInterfaces\WebApi\Controllers\Identities\UserController.cs) to user APIs

- This Controller will use IUserService to interact with Business Domain Service.

Step 8: Done. If users send requests to API. Web API will call UserService, UserService will call UserRepository to serve their requests.

*NOTE:* We are not doing DDD (Domain Driven Design). It's acceptable to put your CRUD in Domain Service. In DDD, your DDD should be managed by Application Layer, this Application Layer can call CRUD from your repositories directly.

### 1.3.4. Database Migration

https://docs.microsoft.com/en-us/ef/core/miscellaneous/cli/dotnet

#### 1.3.4.1. Installation

The Entity Framework Core .NET Command-line Tools are an extension to the cross-platform dotnet command.

Install the EF Core .NET Command-line Tools using these steps in : 
https://docs.microsoft.com/en-us/ef/core/miscellaneous/cli/dotnet#installing-the-tools

#### 1.3.4.2. Add Migration

```bash
#!/bin/bash
# at your solution path
# Change Working Directory to the migration project, 
# which contain Database Configuration (Db Context, connection string for the migration,..)
# This is required. Because dotnet ef command just available for the project which installed the 
# Microsoft.EntityFrameworkCore.Tools.DotNet package

$ cd .\Infrastructures\Infrastructures.DbMigration

# dotnet ef migrations add <migration name>
$ dotnet ef migrations add Init
```

* **dotnet ef migrations add** : tell EF Migration to Generate migration script (by default, It'll generate migration scripts in C# format)
* **... Init ...** : the init part is the name of migration. This name will be added to the MigrationId. MigrationId is an identity to indicate if a migration have been executed.
* **--project ...**: Optional. 
This part tell the migrator where will you store the migration scripts. 


In some project, It requires to review generated SQL script before the deployment, you can generate SQL migration scripts to review by using: 

```
# -i/--idempotent Generate a script that can be used on a database at any migration.
# -o/--output The file to write the result to.
$ dotnet ef migrations script -i -o .\Scripts\db-scheme.sql
```

#### 1.3.4.3. Update Database
```
# at your solution path, Change Working Directory to the migration project, 
# Make sure your connection string is well configured in appsettings.json
$ cd .\Infrastructures\Infrastructures.DbMigration
$ dotnet ef database update
```


#### 1.3.4.4. Seed Data

```
$ cd .\Infrastructures\Infrastructures.SeedData

# dotnet run <connectionString>
$ dotnet run "Server =.\SQLEXPRESS; Database = AspnetBoilerplate; Trusted_Connection = True;"

# If your connection string is defined in \Infrastructures\Infrastructures.SeedData\appsettings.json
# You are able to run "dotnet run" without connectionString
```

### 1.3.5. Angular-cli

```bash
#!/bin/bash
$ ng new ClientApp --skip-tests --routing --style=scss
```

To generate new angular project to:
- Not include test spec when we use ng generate
- Use scss for styles
- Generate routing module


### 1.3.6. Design Pattern

#### 1.3.6.1. Repository  

#### 1.3.6.2. Unit of Work

- You can inject IRepository without UnitOfWork


### 1.3.7. NOTES

Entity vs Object Value
Entity vs Model
Model vs DTO


### 1.3.8. Working with angular form
#### 1.3.8.1. Generic form


Example: Develop Create / Update / View page for User Management

Approach: Create / Update / View will share the same code base, so that I'll reduce our effort on development for each page.

Step 1: Setup routing

```typescript
const routes: Routes = [
  { path: 'list', component: UserListComponent },
  { path: 'form/:mode/:id', component: UserFormComponent }, // View / Edit
  { path: 'form/:mode', component: UserFormComponent }      // Create
];
```

- :mode: it might be Create / Update / View. FormBaseComponent will choose the right action/behavior base on this mode
- :id : id of current record, which will be used in Update / View form

Step 2: Create UserFormComponent, which should be inherit from `FormBaseComponent`, which contains codebase for our form

```typescript
@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent extends FormBaseComponent implements OnInit {
  constructor(protected route: ActivatedRoute,
    protected router: Router,
    protected userService: UserService,
    protected notificationService: NotificationService,
    protected validationService: FormValidationService,
    private _notificationService: NotificationService,
  ) {
    super(route, router, notificationService, userService, validationService);
    super.formOnInit("User", {});
  }

  ngOnInit() {
  }

}
```


Step 3: Create UserService to communicate with our APIs, which should be implement required interface: ICreateFormService, IViewFormService, IUpdateFormService 

```typescript

@Injectable({
  providedIn: 'root'
})
export class UserService implements ICreateFormService, IViewFormService, IUpdateFormService {


  edit(id: any, formData: any): Observable<any> {
    return this._apiHttp.put(`/api/user/${id}`, formData);
  }

  getFormData(id: any): Observable<any> {
    return this._apiHttp.get(`/api/user/${id}`);
  }

  create(formData: any): Observable<any> {
    return this._apiHttp.post(`/api/user`, formData);
  }

  getViewFormUrl(id: any): string {
    return `/user/form/view/${id}`;
  }
  getListPageUrl(): string {
    return `/user/list`;
  }

  constructor(private _apiHttp: AuthHttpService) {

  }
}

```

Step 4: Create HTML page which contains form input

```HTML
<div>
  <form name="loginForm" #mainForm="ngForm" (ngSubmit)="onSubmitForm(mainForm)">

    <div>
      <!-- Create / View -->
      <button *ngIf="!isUpdateFormMode" type="submit" (click)="onNavigateToListPage()">Back to List</button>

      <!-- Update -->
      <button *ngIf="isUpdateFormMode" type="submit" (click)="onNavigateToViewPage()">Cancel</button>

      <!-- Create / Update -->
      <button *ngIf="!isViewFormMode" type="submit">Save</button>

      <!-- View -->
      <a *ngIf="isViewFormMode" routerLink="/user/form/update/{{formData?.id}}" class="btn btn-primary">Update</a>
    </div>


    <div>
      <label for="phoneNumber">Phone Number</label>
      <input type="text" id="phoneNumber" name="phoneNumber" [(ngModel)]="formData.phoneNumber" required [disabled]="isViewFormMode">
      <div [hidden]="!formErrors.phoneNumber" >{{formErrors.phoneNumber}}</div>
    </div>    
  </form>
</div>

```

Step 4.1: Setup form

```html
<form name="loginForm" #mainForm="ngForm" (ngSubmit)="onSubmitForm(mainForm)">
```

- **#mainForm="ngForm"** : which is required. FormBaseComponent will get your form info/members via `#mainForm` instance
- **(ngSubmit)="onSubmitForm(mainForm)** :  onSubmitForm, call submit form action. It'll 


Step 4.2: Setup form toolbar

```HTML
<!-- Create / View -->
<button *ngIf="!isUpdateFormMode" type="submit" (click)="onNavigateToListPage()">Back to List</button>
```

- onNavigateToListPage : will navigate you to the list page, (base on your configuration in UserService.getListPageUrl)

```HTML
<button *ngIf="isUpdateFormMode" type="submit" (click)="onNavigateToViewPage()">Cancel</button>
```

- onNavigateToViewPage: will navigate you to the view page, (base on your configuration in UserService.getViewFormUrl)


Step 4.3: Setup form input

```
<input type="text" id="phoneNumber" name="phoneNumber" [(ngModel)]="formData.phoneNumber" required [disabled]="isViewFormMode">
```

- `[disabled]="isViewFormMode"` : This input will be disabled (read-lonly ) in view Mode

Step 4.4: Setup validation error message: 

```
<div [hidden]="!formErrors.phoneNumber" >{{formErrors.phoneNumber}}</div>
```

- formErrors: all validation error will be stored in `formErrors`. To get validation for control name = phoneNumber. You should use `formErrors.phoneNumber` to get your validation message



## 1.4. References: 
- https://herbertograca.com/2017/08/03/layered-architecture/
- https://www.safaribooksonline.com/library/view/software-architecture-patterns/9781491971437/ch01.html
- https://dzone.com/articles/layered-architecture-is-good
- https://herbertograca.com/2017/09/21/onion-architecture/



[layered-architecture-diagram]: ./Docs/Images/layered-architecture.png "Layer Architecture Diagram"
[onion-architecture-diagram]: ./Docs/Images/onion-architecture.png "Onion Architecture Diagram"