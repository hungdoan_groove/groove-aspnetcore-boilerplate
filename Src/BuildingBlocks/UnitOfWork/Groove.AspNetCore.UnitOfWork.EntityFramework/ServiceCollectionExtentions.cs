﻿using Groove.AspNetCore.Extentions.DependencyInjection;
using Groove.AspNetCore.UnitOfWork;
using Groove.AspNetCore.UnitOfWork.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtentions
    {
        public static IServiceCollection AddUnitOfWork<TDbContext>(this GrooveServiceConfig<IServiceCollection> serviceCollection) where TDbContext : DbContext
        {
            serviceCollection.Instance.AddScoped<IUnitOfWork, UnitOfWork<TDbContext>>();

            serviceCollection.Instance.AddTransient(typeof(ISpecification<>), typeof(Specification<>));

            serviceCollection.Instance.AddTransient(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            serviceCollection.Instance.AddTransient(typeof(IGenericRepository<,>), typeof(GenericRepository<,>));

            return serviceCollection.Instance;
        }
    }
}