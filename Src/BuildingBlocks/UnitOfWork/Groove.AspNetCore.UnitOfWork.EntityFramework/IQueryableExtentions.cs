﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Groove.AspNetCore.UnitOfWork.EntityFramework
{
    public static class IQueryableExtentions
    {
        public static IQueryable<TEntity> ApplySpecification<TEntity>(
            this IQueryable<TEntity> queryable,
            Action<ISpecification<TEntity>> querySpecs) where TEntity : class
        {
            var specs = new Specification<TEntity>();
            querySpecs(specs);

            foreach (var propertyExpression in specs.IncludesByExpression)
            {
                queryable = queryable.Include(propertyExpression);
            }

            foreach (var propertyName in specs.IncludesByName)
            {
                queryable = queryable.Include(propertyName);
            }

            foreach (var criteria in specs.Criterias)
            {
                queryable = queryable.Where(criteria);
            }

            if (specs.OrderByExpression != null)
            {
                if (specs.OrderByDirection == OrderByDirection.Ascending)
                {
                    queryable = queryable.OrderBy(specs.OrderByExpression);
                }

                if (specs.OrderByDirection == OrderByDirection.Decending)
                {
                    queryable = queryable.OrderByDescending(specs.OrderByExpression);
                }
            }

            return queryable;
        }
    }
}