﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Groove.AspNetCore.UnitOfWork.EntityFramework
{
    public class Specification<TEntity> : ISpecification<TEntity> where TEntity : class
    {
        public Specification()
        {
            Criterias = new List<Expression<Func<TEntity, bool>>>();
            IncludesByExpression = new List<Expression<Func<TEntity, object>>>();
            IncludesByName = new List<string>();
        }

        public List<Expression<Func<TEntity, bool>>> Criterias { get; set; }
        public List<Expression<Func<TEntity, object>>> IncludesByExpression { get; set; }

        public List<string> IncludesByName { get; set; }
        public OrderByDirection OrderByDirection { get; set; }
        public Expression<Func<TEntity, object>> OrderByExpression { get; set; }
        public void Include(Expression<Func<TEntity, object>> property)
        {
            IncludesByExpression.Add(property);
        }

        public void Include(string propertyName)
        {
            IncludesByName.Add(propertyName);
        }

        public void OrderBy(Expression<Func<TEntity, object>> orderBy, OrderByDirection direction)
        {
            OrderByExpression = orderBy;
            OrderByDirection = direction;
        }

        public void Where(Expression<Func<TEntity, bool>> criteria)
        {
            Criterias.Add(criteria);
        }
    }
}