﻿using Groove.AspNetCore.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Groove.AspNetCore.UnitOfWork.EntityFramework
{
    public class GenericRepository<TEntity> : RepositoryBase<TEntity> where TEntity : class
    {
        public GenericRepository(IUnitOfWork uow)
            : base((DbContext)uow.GetContext())
        {
        }
    }

    public class GenericRepository<TEntity, TKey> : RepositoryBase<TEntity, TKey> where TEntity : class, IEntity<TKey>
    {
        public GenericRepository(IUnitOfWork uow)
            : base((DbContext)uow.GetContext())
        {
        }
    }
}