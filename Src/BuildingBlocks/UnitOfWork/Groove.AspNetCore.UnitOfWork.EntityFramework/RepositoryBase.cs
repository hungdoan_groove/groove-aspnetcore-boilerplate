﻿using Groove.AspNetCore.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Groove.AspNetCore.UnitOfWork.EntityFramework
{
    public abstract class RepositoryBase<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected DbContext context;
        protected DbSet<TEntity> dbSet;

        public RepositoryBase(DbContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        public async Task<bool> AnyAsync(Action<ISpecification<TEntity>> query)
        {
            return await this.dbSet.AsQueryable().ApplySpecification(query).AnyAsync();
        }

        public virtual void Create(TEntity entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public async Task<TEntity> FirstOrDefaultAsync(Action<ISpecification<TEntity>> query)
        {
            return await this.dbSet.AsQueryable().ApplySpecification(query).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await this.dbSet.ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync(Action<ISpecification<TEntity>> query)
        {
            return await this.dbSet.AsQueryable().ApplySpecification(query).ToListAsync();
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }
    }

    public class RepositoryBase<TEntity, TKey> : RepositoryBase<TEntity>, IGenericRepository<TEntity, TKey> where TEntity : class, IEntity<TKey>
    {
        public RepositoryBase(DbContext context)
            : base(context)
        {
        }

        public virtual Task<TEntity> GetEntityByIdAsync(TKey id)
        {
            return dbSet.SingleOrDefaultAsync(p => p.Id.Equals(id));
        }
    }
}