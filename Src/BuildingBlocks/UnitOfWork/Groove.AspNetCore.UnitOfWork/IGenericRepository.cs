﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Groove.AspNetCore.UnitOfWork
{
    public interface IGenericRepository<TEntity>
           where TEntity : class
    {
        Task<bool> AnyAsync(Action<ISpecification<TEntity>> query);

        void Create(TEntity entity);

        void Delete(TEntity entityToDelete);

        Task<TEntity> FirstOrDefaultAsync(Action<ISpecification<TEntity>> query);

        Task<IEnumerable<TEntity>> GetAllAsync();

        Task<IEnumerable<TEntity>> GetAllAsync(Action<ISpecification<TEntity>> query);

        void Update(TEntity entityToUpdate);
    }

    public interface IGenericRepository<TEntity, TKey>
        : IGenericRepository<TEntity>
        where TEntity : class
    {
        Task<TEntity> GetEntityByIdAsync(TKey id);
    }
}