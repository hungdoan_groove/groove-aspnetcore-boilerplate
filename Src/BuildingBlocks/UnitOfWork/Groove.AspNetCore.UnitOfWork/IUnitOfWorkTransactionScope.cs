﻿using System;

namespace Groove.AspNetCore.UnitOfWork
{
    public interface IUnitOfWorkTransactionScope : IDisposable
    {
        IUnitOfWorkTransactionScope BeginTransaction();

        void Commit();

        void Rollback();
    }
}