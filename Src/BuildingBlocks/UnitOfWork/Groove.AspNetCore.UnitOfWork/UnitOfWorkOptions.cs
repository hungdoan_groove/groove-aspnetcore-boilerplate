﻿using System;

namespace Groove.AspNetCore.UnitOfWork
{
    public class UnitOfWorkOptions
    {
        public TimeSpan? Timeout { get; set; }
    }
}