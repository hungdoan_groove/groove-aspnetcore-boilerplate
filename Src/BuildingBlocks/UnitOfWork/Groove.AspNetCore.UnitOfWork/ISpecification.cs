﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Groove.AspNetCore.UnitOfWork
{
    public enum OrderByDirection
    {
        Ascending,
        Decending
    }

    public interface ISpecification<TEntity> where TEntity : class
    {
        List<Expression<Func<TEntity, bool>>> Criterias { get; }
        List<Expression<Func<TEntity, object>>> IncludesByExpression { get; }
        List<string> IncludesByName { get; }
        OrderByDirection OrderByDirection { get; }
        Expression<Func<TEntity, object>> OrderByExpression { get; }
        void Include(Expression<Func<TEntity, object>> property);

        void Include(string include);

        void OrderBy(Expression<Func<TEntity, object>> orderBy, OrderByDirection direction);

        void Where(Expression<Func<TEntity, bool>> criteria);
    }
}