﻿using Groove.AspNetCore.Extentions.DependencyInjection;
using Groove.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class MvcBuilderExtentions
    {
        public static IMvcBuilder AddGrooveMvcOptions(this IMvcBuilder mvcBuilder, Action<GrooveServiceConfig<IMvcBuilder>> options)
        {
            options(new GrooveServiceConfig<IMvcBuilder>(mvcBuilder));
            return mvcBuilder;
        }


        public static IMvcBuilder AddExceptionFilter(this GrooveServiceConfig<IMvcBuilder> mvcBuilder)
        {
            mvcBuilder.Instance.AddMvcOptions(opt => opt.Filters.Add(new ExceptionFilter()));
            return mvcBuilder.Instance;
        }

        public static IMvcBuilder AddJsonOptions(this GrooveServiceConfig<IMvcBuilder> mvcBuilder)
        {
            mvcBuilder.Instance.AddJsonOptions(x =>
            {
                x.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                x.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                x.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Error;
            });

            return mvcBuilder.Instance;
        }
    }
}
