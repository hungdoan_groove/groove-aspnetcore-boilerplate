﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Groove.AspNetCore.Mvc.ActionResults
{
    public class ValueDto
    {
        public object Value { get; set; }
        public ValueDto(object value)
        {
            this.Value = value;
        }
    }
}
