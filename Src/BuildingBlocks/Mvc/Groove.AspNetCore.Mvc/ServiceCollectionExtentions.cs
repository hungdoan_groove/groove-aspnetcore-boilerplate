﻿using Groove.AspNetCore.Extentions.DependencyInjection;
using Groove.AspNetCore.Identity;
using Groove.AspNetCore.Mvc;
using Groove.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtentions
    {
        public static IServiceCollection AddRequestIdentityService(this GrooveServiceConfig<IServiceCollection> serviceCollection)
        {
            serviceCollection.Instance.AddScoped<IRequestIdentityService, RequestIdentityService>();

            return serviceCollection.Instance;
        }

        public static IServiceCollection AddCors(this GrooveServiceConfig<IServiceCollection> serviceCollection)
        {
            return serviceCollection.Instance.AddCors(options => options.AddPolicy(CorsPolicies.AllowAny, builder =>
             {
                 builder.AllowAnyOrigin()
                 .AllowAnyMethod()
                 .AllowAnyHeader();
             }));
        }

        public static IServiceCollection AddCors(this GrooveServiceConfig<IServiceCollection> serviceCollection, Action<CorsOptions> setupAction)
        {
            return serviceCollection.Instance.AddCors(setupAction);
        }
    }
}