﻿using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;

namespace Groove.AspNetCore.Identity
{
    public class RequestIdentityService : IRequestIdentityService
    {
        private readonly IHttpContextAccessor _context;

        public RequestIdentityService(IHttpContextAccessor context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public string GetCurrentUserId()
        {
            return _context.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
        }

        public T GetCurrentUserId<T>()
        {
            return (T)Convert.ChangeType(GetCurrentUserId(), typeof(T));
        }

        public string GetCurrentUserName()
        {
            return _context.HttpContext.User.FindFirst(ClaimTypes.Name).Value;
        }

        public UserIdentity<T> GetCurrentIdentity<T>()
        {
            return new UserIdentity<T>
            {
                Id = GetCurrentUserId<T>(),
                UserName = GetCurrentUserName()
            };
        }
    }
}