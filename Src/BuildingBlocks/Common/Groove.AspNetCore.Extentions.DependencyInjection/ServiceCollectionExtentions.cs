﻿using Groove.AspNetCore.Extentions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtentions
    {
        public static IServiceCollection AddGroove(this IServiceCollection services, Action<GrooveServiceConfig<IServiceCollection>> options)
        {
            options(new GrooveServiceConfig<IServiceCollection>(services));
            return services;
        }
    }
}
