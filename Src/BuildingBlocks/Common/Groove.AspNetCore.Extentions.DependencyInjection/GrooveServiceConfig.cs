﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Groove.AspNetCore.Extentions.DependencyInjection
{
    /// <summary>
    /// Implementation should use namespace : Microsoft.Extensions.DependencyInjection
    /// </summary>
    public class GrooveServiceConfig<T>
    {
        public readonly T Instance;
        public GrooveServiceConfig(T instance)
        {
            this.Instance = instance;
        }
    }
}
