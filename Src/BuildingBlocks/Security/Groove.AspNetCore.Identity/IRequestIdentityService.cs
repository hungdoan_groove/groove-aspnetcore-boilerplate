﻿using System;

namespace Groove.AspNetCore.Identity
{
    public interface IRequestIdentityService
    {
        string GetCurrentUserId();

        T GetCurrentUserId<T>();

        string GetCurrentUserName();

        UserIdentity<T> GetCurrentIdentity<T>();
    }
}