
<!-- TOC -->

- [1. Clean architecture](#1-clean-architecture)

<!-- /TOC -->
# 1. Clean architecture

![clean-architecture-design][clean-architecture-design]
There are 2 kind of business

- Application Specific logic: your use cases
- Enterprise business logic: Business risky rules, business calculation, business validation...

Your application service are able to execute Enterprise business logic to perform an use case.



Inner application
- Boundary: to translate your entity into response model or from request model to entity


- Interactor


Outer application

- Controller : command
- Presenter : query

Layers

- 

Business logic are: 
- Validations
- Calculations
- Business rules
- ...



Normally we won't use Domain Service. But you will need Domain Service in case you have to integrate with multiple Aggregate Root


From Evans’ DDD, a good Service has these characteristics:

- The operation relates to a domain concept that is not a natural part of an Entity or Value Object
- The interface is defined in terms of other elements in the domain model
- The operation is stateless

and, Do not use Domain Service too much, we usually end up with an anemic model with behavior in services

- https://herbertograca.com/2017/09/28/clean-architecture-standing-on-the-shoulders-of-giants/
- https://plainionist.github.io/Implementing-Clean-Architecture-Controller-Presenter/
- http://rodschmidt.com/the-clean-architecture-an-introduction/
- http://cleancodejava.com/uncle-bob-payroll-case-study-full-implementation/
- https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html (by  Robert C. Martin - Author of Clean Architecture)

[clean-architecture-design]: ./Docs/Images/clean-architecture-design.png "Architecture Diagram"
- https://matthiasnoback.nl/2017/08/layers-ports-and-adapters-part-3-ports-and-adapters/
- https://matthiasnoback.nl/2017/08/layers-ports-and-adapters-part-2-layers/
- https://lostechies.com/jimmybogard/2008/08/21/services-in-domain-driven-design/