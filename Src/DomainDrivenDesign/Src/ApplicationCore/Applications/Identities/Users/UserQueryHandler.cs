﻿using Applications.Identities.Users.Queries;
using Applications.Identities.Users.Queries.ResponseModel;
using Applications.Identities.Users.QueryRepositories;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Applications.Identities.Users
{
    public class UserQueryHandler :
        IRequestHandler<GetUserDetailQuery, UserDetailViewModel>,
        IRequestHandler<GetUserListQuery, UserListViewModel>
    {
        private readonly IUserQueryService _userQueryService;

        public UserQueryHandler(IUserQueryService userQueryRepository)
        {
            _userQueryService = userQueryRepository;
        }

        public Task<UserDetailViewModel> Handle(GetUserDetailQuery request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<UserListViewModel> Handle(GetUserListQuery request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}