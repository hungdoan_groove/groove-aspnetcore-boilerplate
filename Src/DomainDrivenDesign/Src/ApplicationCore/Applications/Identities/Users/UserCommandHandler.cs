﻿using Applications.Identities.Users.Commands;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Applications.Identities.Users
{
    public class UserCommandHandler :
        IRequestHandler<CreateNewUserCommand>,
        IRequestHandler<UpdateUserCommand>,
        IRequestHandler<ActivateUserCommand>,
        IRequestHandler<DeactivateUserCommand>
    {
        public UserCommandHandler()
        {
        }

        public Task<Unit> Handle(CreateNewUserCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<Unit> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<Unit> Handle(ActivateUserCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<Unit> Handle(DeactivateUserCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}