﻿using Applications.Identities.Users.Queries.ResponseModel;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Applications.Identities.Users.Queries
{
    public class GetUserListQuery : IRequest<UserListViewModel>
    {
    }
}