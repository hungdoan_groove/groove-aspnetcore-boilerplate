
<!-- TOC -->

- [READ ME](#read-me)

<!-- /TOC -->

# READ ME

In this project, we are going to implement simple version of Clean Architecture. 

Clean Architecture also is the name of a book was introduced by Robert C. Martin in his Clean Series (Clean Code, The Clean Coder, Clean Architecture,...)

Clean Architecture should have the following characteristics:

- Independent of frameworks
- Testable
- Independent of the UI
- Independent of the database
- Independent of any external agency


There are 4 basic Layer/Circles in Clean Architecture

- Entities: which contain enterprise-wide business rules. You can think of them as about Domain Entities a la DDD.
- Use Cases: which contain application-specific business rules. These would be counterparts to Application Services with the caveat that each class should focus on one particular Use Case
- Interface Adapters: which contain adapters to peripheral technologies. Here, you can expect MVC, Gateway implementations and the like.
- Frameworks & Drivers: which contain tools like databases or framework. By default, you don’t code too much in this layer, but it’s important to clearly state the place and priority that those tools have in your architecture.

References: 
- https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html (by  Robert C. Martin - Author of Clean Architecture)
- https://crosp.net/blog/software-architecture/clean-architecture-part-2-the-clean-architecture/ (must read)
- Robert C. Martin - Clean Architecture https://vimeo.com/43612849
- Robert C. Martin - Clean Architecture and Design https://www.youtube.com/watch?v=Nsjsiz2A9mg
- https://herbertograca.com/2017/09/21/onion-architecture/
- https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/
- https://gist.github.com/ygrenzinger/14812a56b9221c9feca0b3621518635b
- https://herbertograca.com/2017/09/28/clean-architecture-standing-on-the-shoulders-of-giants/
- http://blog.ploeh.dk/2013/12/03/layers-onions-ports-adapters-its-all-the-same/
- https://herbertograca.com/2017/08/03/layered-architecture/