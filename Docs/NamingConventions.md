
# Naming Conventions

<!-- TOC -->

- [Naming Conventions](#naming-conventions)
    - [CRUD use cases](#crud-use-cases)
        - [Create new User](#create-new-user)
        - [Update User info](#update-user-info)
        - [Change User Status (activate / deactivate / soft-delete)](#change-user-status-activate--deactivate--soft-delete)
        - [Get User List](#get-user-list)
        - [Get User Detail](#get-user-detail)

<!-- /TOC -->

## CRUD use cases

### Create new User

```text
Press Create Button 
-> HTTP POST /api/user
-> UserService.Create() 
-> UserRepositoty.Create()
-> UnitOfWork.SaveChange
```

### Update User info

```text
Press Update Button 
-> HTTP PUT /api/user/{id}
-> UserService.Update() 
-> UserRepositoty.GetEntity()
-> UserRepositoty.Update() 
-> UnitOfWork.SaveChange()

```

### Change User Status (activate / deactivate / soft-delete)

```text
Press Activate/Deactivate/Delete Button 
-> HTTP PUT /api/user/{id}/activate...
-> UserService.UpdateStatus() 
-> UserRepositoty.GetEntity()
-> UserRepositoty.Update() 
-> UnitOfWork.SaveChange()

```

### Get User List


```text
-> HTTP GET  /api/user
-> UserService.GetUserList() 
-> UserRepositoty.GetUserListAsync()

```


### Get User Detail

```text
-> HTTP GET  /api/user/{id}
-> UserService.GetUserDetail(id) 
-> UserRepositoty.GetUserDetail()

```