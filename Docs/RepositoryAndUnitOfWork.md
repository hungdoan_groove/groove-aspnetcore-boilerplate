<!-- TOC -->

- [1. Repository and Unit of Work](#1-repository-and-unit-of-work)
    - [1.1. Overview](#11-overview)
    - [1.2. The difference between the Repository pattern and the legacy Data Access class (DAL class) pattern](#12-the-difference-between-the-repository-pattern-and-the-legacy-data-access-class-dal-class-pattern)
    - [1.3. Naming convention](#13-naming-convention)
        - [1.3.1. Repository class/interface](#131-repository-classinterface)
        - [1.3.2. Methods](#132-methods)

<!-- /TOC -->

# 1. Repository and Unit of Work

## 1.1. Overview

In this implementation, we are using `Repository` and Unit Of Work to manage transaction between our Application and Storage.

- Repository : You can treat this pattern as a higher level of DTO (Data Transfer Object), this Repository will serve our `Domain` layer for it's business (providing data access). This Repository should return Domain Object Only (Entities, Domain Model,...)

- Unit of Work: Unit Of Work is not a part of Repository patterns. But We use Unit of Work to manage actions which are triggered from Repositories. Unit Of Work will combine all actions (Update/Create/Delete) from Repositories and send to Storage at once.

ORM/Entity Framework vs Repository + UnitOfWork

Example:

```C#
userRepository.Create(new User{Name = "My Name"});
storeRepository.Create(new Store{Name = "My Store"});

// All actions from userRepository wont be applied until `SaveChanges` was executed
unitOfWork.SaveChanges();

```

In this implementation. We are using EntityFramework as a ORM to work with database. By default, Database Context registered via `services.AddDbContext` is Scoped (DBContext is created once per request)

So that, UnitOfWork and Repository are sharing the same DbContext

In this implementation, there are 2 type of Repository:

- Generic Repository: IGenericRepository<User>. This is a generic repository, which contains all common feature of a repository: find an Entity, Insert, Update, Delete an Entity record. But Sometime, Generic repository would not able to provide required API, in this case you should implement your own repository in your Specific Repository instance.
- Specific Repository: IUserRepository. Get User Detail, Get User List,.. because these function require complex joining / filtering which can not be done with Generic Repository


## 1.2. The difference between the Repository pattern and the legacy Data Access class (DAL class) pattern

A data access object directly performs data access and persistence operations against storage. A repository marks the data with the operations you want to perform in the memory of a unit of work object (as in EF when using the DbContext class), but these updates aren't performed immediately.

A unit of work is referred to as a single transaction that involves multiple insert, update, or delete operations. In simple terms, it means that for a specific user action, such as a registration on a website, all the insert, update, and delete transactions are handled in a single transaction. This is more efficient than handling multiple database transactions in a chattier way.


(Ref: https://docs.microsoft.com/en-us/dotnet/standard/microservices-architecture/microservice-ddd-cqrs-patterns/infrastructure-persistence-layer-design)


## 1.3. Naming convention

### 1.3.1. Repository class/interface

1. Class name should be suffixed by `Repository`

```C#
IUserRepository
IProductRepository
```

### 1.3.2. Methods

1. Create vs Add vs Insert
- Create: Create new object
- Add : Add an Object to an existence object
- Insert: It's the same as `Add`, but the order of Object is importance.

  But I prefer to use Create to follow CRUD.

2. Update vs Modify vs Edit vs Change
- Update: Update data
- Modify: change design & structure of object
- Edit: 
- Change:

  But I prefer to use Update to follow CRUD.

3. Delete vs Remove 

  But I prefer to use Update to follow CRUD.

4. Get vs Find
I prefer to use
- Get: to get one object
- GetAll: to get a list of object
