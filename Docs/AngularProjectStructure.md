<!-- TOC -->

- [1. Angular Project Structure](#1-angular-project-structure)
    - [1.1. Pages vs Components](#11-pages-vs-components)

<!-- /TOC -->
# 1. Angular Project Structure

## 1.1. Pages vs Components

Pages or Components are components in Angular. But in our project we can define that:

- Page : Page is a component that hold a view. Technically, a page is a component that was defined in a Routing rule.
- Component : Is a UI component in a page/view


References:
[1] https://angular.io/guide/styleguide
[2] https://medium.com/@motcowley/angular-folder-structure-d1809be95542