
Key benefit of DDD: 

- DDD model will tell you the main business/key feature of your application.
- Each method of Domain model would tell you about: What is the business is? Why we need this method (to reduce memory lost issue)


Presentation
Application
DomainModel
Infrastructure (for persistence ignorance)

CRUD based vs Domain Driven Based

The basic different is that in CRUD base, you are able to update all properties at the same time.

But in DDD, It's better suited for task based/event based UI. 

For example, we have user object

```
User {
    Phone,
    Email,
    Address
}
```

What is the business benefit of update all of those fields together? Why should they do that?

Normally, They just want to update only one field with a purpose. That is the reason to use DDD to describe the business purpose of your application.


## Repository

- One repository per aggregate
- One aggregate root per repository

## Value Object

There are two main characteristics for value objects:
- They have no identity.
- They are immutable.